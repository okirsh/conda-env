#!/usr/bin/env bash

function create_module_file {
    envfile_path=$1
    env_name=`echo $envfile_path | awk -F/ '{print $(NF-1)}'`
    env_filename=`echo $envfile_path | awk -F/ '{print $(NF)}'`
    env_version="${env_filename%.*}"
    env_path=${CONDA_HOME}/envs/${env_name}-${env_version}

    mkdir -p "${MODULE_FILE_PATH}/${env_name}"
    module_file="${MODULE_FILE_PATH}/${env_name}/${env_version}"
    cp ci/modulefile $module_file
    echo "conflict ${env_name}" >> $module_file
    if [ -d $env_path/bin ]; then
        echo "prepend-path PATH ${env_path}/bin" >> $module_file
    fi
    if [ -d $env_path/include ]; then
        echo "prepend-path CPATH ${env_path}/include" >> $module_file
    fi
    if [ -d $env_path/lib ]; then
        echo "prepend-path LD_LIBRARY_PATH ${env_path}/lib" >> $module_file
    fi
    if [ -d $env_path/man ]; then
        echo "prepend-path PATH ${env_path}/man" >> $module_file
    fi
}

export PATH=$CONDA_HOME/bin:$PATH
which conda

#because gitlab-ci will only keep the artifacts 5 minutes
change_env_list="change_env_backup.list"
cp $1 $change_env_list

exit_code=0

# creating envs
for envfile in */*/*.y*ml; do
    if grep -Fxq "$envfile" $change_env_list
    then
      echo "$envfile: creating..."
      conda env create -f $envfile 2> stderr
      if [ $? -eq 0 ]; then
          create_module_file $envfile
          echo "$envfile: created"
      else
          if grep --quiet "CondaValueError: prefix already exists:" stderr; then
              echo "$envfile: prefix_already_exists"
              if grep --quiet "^${envfile}$" $change_env_list; then
                  echo "$envfile: updating..."
                  conda env update -f $envfile 2> stderr
                  if [ $? -eq 0 ]; then
                      create_module_file $envfile
                      echo "$envfile: updated"
                  else
                      echo -n "Updating $envfile: error"
                      cat stderr
                      exit_code=1
                  fi
              fi
          else
              echo -n "Creating $envfile: error"
              cat stderr
              exit_code=1
          fi
      fi
    fi
done

exit $exit_code
