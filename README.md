# Conda environment

[![pipeline status](https://192.168.103.4/taskforce-nncr/conda-env/badges/dev/pipeline.svg)](https://192.168.103.4/taskforce-nncr/conda-env/commits/dev)

This collaborative repository makes it possible to securely install Conda packages within the [IFB-Core-Cluster](https://www.france-bioinformatique.fr/en/cluster)

## Mechanisms
### Conda
Conda is a package and environment manager: https://docs.conda.io/en/latest/

It's easy to install tools on a HPC infrastructure (or on any computer). Indeed, a software can have many dependencies which also have many dependencies themselves... Conda will handle that for us. Each package will be installed with all the  packages it needs (as well as the system libraries).

And since tools are installed in dedicated semi-isolated environments, you control which version is installed and this ensures reproducibility.


### Git / GitLab
[Git](https://en.wikipedia.org/wiki/Git) is a version-control system for tracking changes in code or in any text files.

GitLab is a web user interface on top of git. It provides different views and editors which allow users to do almost any actions around git:
 * edit text files
 * submit changes
 * merge contributions
 * ...

### Continuous Integration (CI)
We try to avoid running manual commands on the infrastructure. A [Continuous Integration](https://en.wikipedia.org/wiki/Continuous_integration) robot will handle that for us. For each contribution this "robot" will launch the checking steps on our different environments.

### The steps
1. You contribute (on a dedicated branch)
2. You submit a Merge Request: this is how you contribute to the git repository
3. The CI robot tries to install on our preprod infrastructure the Conda environment you have just submitted
4. If the test is flagged green, one of the maintainers will merge your contribution with the master branch
5. The CI robot installs your Conda environment on the production server

## How to contribute
You can submit code either through the GitLab interface or with git commands directly if you are familiar with git. 
Here we will focus on the GitLab interface.

### Create a new environment file
0. Check that the package you need is available in Conda: https://anaconda.org/ (or in any search engine: `conda +my_tool`)
1. From this page [conda-env](https://gitlab.com/ifb-elixirfr/cluster/conda-env), click on the **tools** folder.
2. Add a new file with the **+** -> **New file**
3. As `File name`, indicate a path using the naming convention `my_tool/version.yml` (ie: `fastqc/0.11.7.yml`)
4. As for the text itself, here is a simple example:

```yml
name: fastqc-0.11.7
channels:
  - conda-forge
  - bioconda
  - default
dependencies:
  - bioconda::fastqc=0.11.7
```
If the package comes from an other channel, add it in the `channels` list and add it as a prefix in the `dependencies` list.
If you are still unsure of the syntax check the many examples you will find in the repository.

5. As **Commit message** follow the naming convention my_tool-version (ie: `fastqc-0.11.7`)
6. **Commit changes** green button
7. **Submit merge request** green button
8. Check the CI pipeline message. If you need to make any change, just use your new branch and edit your file again.


## Why contribute
Why contribute to this repository instead of installing Conda on your own?
Because:
 * It saves disk space on your own quota. So it makes both economic and ecological sense.
 * You contribute to make new tools available to a larger community
